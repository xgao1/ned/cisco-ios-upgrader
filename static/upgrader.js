$(document).ready(function() {
    // enable the filename to appear on select
    // $(".custom-file-input").change(function () {
    //     let filename = null;
    //     filename = $(this).val().split('\\').pop();
    //     if (filename) {
    //         $(this).siblings(".custom-file-label").addClass('selected').html(filename);
    //     }
    // });
    $("input[name='net']").change(function() {
        if ($(this).val() == 'off') {
            $("#off_net_prompt_div").show();
            $("#on_net_prompt_div").hide();
        } else {
            $("#off_net_prompt_div").hide();
            $("#on_net_prompt_div").show();
        }

        // go back to step #1
        $("#s1_div").show();
        $("#switch_div").css('visibility', 'visible');
        $("#s2_div, #s3_div, #s4_div, #invalid_ip_odiv, #valid_ip_odiv").hide();
        $("input[name='invalid_ip_inp'], input[name='valid_ip_inp']").remove();
        $("#email").val('').off('change').removeClass('is-invalid');
        $(".progress-bar").width('0');
        // validIPs = null;
        // invalidIpsV = null;
        $("#inps_div input").trigger('change');
    });

    $("#num_devi").change(function() {
        if ($(this).val() <= 0) {
            $(this).val(1);
            $("#inps_div input:not(:first)").remove();
        } else {
            let numOfInps = $("#inps_div input").length;
            if ($(this).val() > numOfInps) {
                for (let i = 0; i < $(this).val() - numOfInps; i++) {
                    $("#inps_div").append('<input type="text" placeholder="X.X.X.X" class="form-control">');
                }
            } else {
                $("#inps_div input").slice($(this).val() - numOfInps).remove();
            }
        }
        $("#devi_model, #radio_inp, #inps_div input").removeClass('is-invalid').off('change');
        $("#radio_inp").addClass('border border-white');
    });

    $("#switch1").change(function() {
        if ($("#switch1:checked").val() == 'on') {
            mode = 'f';
        } else {
            mode = 'i';
        }
        // $("#num_devi").val(1);
        // $("#inps_div input:not(:first)").remove();
        $("#devi_model, #radio_inp, #inps_div input, #f_inp").removeClass('is-invalid');
        $("#radio_inp").addClass('border border-white');
        $("#devi_model, #inps_div input, #f_inp").off('change');
        $("#s1_i_div, #s1_f_div").toggle();
    });

    $("[data-toggle='tooltip']").tooltip();

    $("#proceed_btn1").click(function() {
        console.log($("#devi_model").val());
        console.log($("input[name='net']:checked").val())
            // if (isFileNone) {
            //     toast.fire({
            //         icon: 'warning',
            //         title: 'Your file has no IP address'
            //     });
            //     return;
            // }

        let countOfInvalidIn = 0;
        if (!$("#devi_model").val()) {
            countOfInvalidIn++;
            $("#devi_model").addClass('is-invalid');
        } else {
            $("#devi_model").removeClass('is-invalid');
        }

        if (!$("input[name='net']:checked").val()) {
            countOfInvalidIn++;
            $("#radio_inp").addClass('is-invalid');
            $("#radio_inp").removeClass('border border-white');
        } else {
            $("#radio_inp").addClass('border border-white');
            $("#radio_inp").removeClass('is-invalid');
        }

        $("#devi_model").change(function() {
            if ($(this).val()) {
                $(this).removeClass('is-invalid');
            } else {
                $(this).addClass('is-invalid');
            }
        });

        $("input[name='net']").change(function() {
            $("#radio_inp").addClass('border border-white');
            $("#radio_inp").removeClass('is-invalid');
        });

        if (mode == 'f') {
            if (!isInpFileValid('f_inp')) {
                countOfInvalidIn++;
                $("#f_inp").addClass('is-invalid');
            } else {
                $("#f_inp").removeClass('is-invalid');
            }

            $("#f_inp").change(function() {
                if (isInpFileValid($(this).attr('id'))) {
                    $(this).removeClass('is-invalid');
                } else {
                    $(this).addClass('is-invalid');
                }
                // isFileNone = false;
            });
        } else {
            if (!$("input[name='net']:checked").val()) {
                $("#inps_div input").addClass('is-invalid');
                countOfInvalidIn++;
            } else {
                let $inps = $("#inps_div input");
                for (let i = 0; i < $inps.length; i++) {
                    if (!isInpValidIp($inps[i].value, $("input[name='net']:checked").val())) {
                        $($inps[i]).addClass('is-invalid');
                        countOfInvalidIn++;
                    }
                }
            }
            $("#inps_div input").change(function() {
                if (!$("input[name='net']:checked").val() || !isInpValidIp($(this).val(), $("input[name='net']:checked").val())) {
                    $(this).addClass('is-invalid');
                } else {
                    $(this).removeClass('is-invalid');
                }
            });
        }

        if (countOfInvalidIn > 0) {
            return;
        }

        let net = $("input[name='net']:checked").val();
        if (mode == 'f') {
            // fd.append('file', $("#f_inp"));
            $(this).prop('disabled', true).html('Pr<span class="spinner-grow spinner-grow-sm"></span>ceeding');
            let fd = new FormData($("form")[0]);
            $.post({
                url: `/processInFile?net=${net}`,
                data: fd,
                processData: false,
                contentType: false,
                success: function(result, status, jqxhr) {
                    $("#proceed_btn1").prop('disabled', false).html('Proceed');
                    // console.log(status, jqxhr.status)
                    let IPs = result['IPs'];
                    let numOfIPs = IPs.length;
                    if (!numOfIPs) {
                        toast.fire({
                            icon: 'warning',
                            title: 'Your file has no IP address'
                        }).then(function() {
                            $("#f_inp").addClass('is-invalid');
                            // isFileNone = true;
                        });
                        return;
                    }
                    // isFileNone = false;
                    let invalidIPs = result['invalid_IPs'];
                    validIPs = result['valid_IPs']; // nonlocal
                    let numOfInvalidIPs = invalidIPs.length;
                    let numOfValidIPs = validIPs.length;

                    $("#info_bd_div").text(numOfIPs);
                    $("#danger_bd_div").text(numOfInvalidIPs);
                    $("#success_db_div").text(numOfValidIPs);

                    if (numOfInvalidIPs) {
                        for (let i = 0; i < numOfInvalidIPs; i++) {
                            $("#invalid_ip_div").append(`<input name="invalid_ip_inp" value="${invalidIPs[i]}" class="form-control w-25 is-invalid">`);
                        }
                        $("#invalid_ip_div input").change(function() {
                            if (isInpValidIp($(this).val(), $("input[name='net']:checked").val())) {
                                $(this).removeClass('is-invalid');
                            } else {
                                $(this).addClass('is-invalid');
                            }
                        });
                        $("#invalid_ip_odiv button").click(function() {
                            $("#info_bd_div").text($("#success_db_div").text());
                            $("#danger_bd_div").text('0');
                            $("input[name='invalid_ip_inp']").remove();
                            $("#invalid_ip_odiv").hide();
                            if (!validIPs || !validIPs.length) {
                                toast.fire({
                                    icon: 'warning',
                                    title: 'You just deleted all IP addresses'
                                }).then(() => {
                                    $("input[name='net']").trigger('change');
                                });
                            }
                        });
                        $("#invalid_ip_odiv").show();
                    }
                    if (numOfValidIPs) {
                        for (let i = 0; i < numOfValidIPs; i++) {
                            $("#valid_ip_div").append(`<input name="valid_ip_inp" value="${validIPs[i]}" class="form-control w-25" disabled>`);
                        }
                        $("#valid_ip_odiv").show();
                    }

                    $("#s1_div").hide();
                    $("#switch_div").css('visibility', 'hidden');
                    $("#s2_div").show();
                    $(".progress-bar").css('width', '33%')
                },
            });
        } else {
            let $inps = $("#inps_div input");
            for (let i = 0; i < $inps.length; i++) {
                validIPsModei.push($inps[i].value);
            }

            $("#s1_div").hide();
            $("#switch_div").css('visibility', 'hidden');
            $("#s3_div").show();
            $(".progress-bar").css('width', '50%')
        }
    });

    $("#proceed_btn2").click(function() {
        let $invalidIPInps = $("input[name='invalid_ip_inp']");
        if (!$invalidIPInps.length && !validIPs.length) {
            return;
        }

        invalidIpsV = [];
        for (let i = 0; i < $invalidIPInps.length; i++) {
            if (!isInpValidIp($invalidIPInps[i].value, $("input[name='net']:checked").val())) {
                return;
            }
            invalidIpsV.push($invalidIPInps[i].value);
        }
        $("#s2_div").hide();
        $("#s3_div").show();
        $(".progress-bar").css('width', '66%')
    });

    $("#proceed_btn3").click(function() {
        let emailRe = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
        let email = $("#email").val();
        $("#email").change(function() {
            console.log(emailRe.test($(this).val()))
            if (!emailRe.test($(this).val())) {
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
            }
        });
        if (!emailRe.test(email)) {
            $("#email").addClass('is-invalid');
            return;
        } else {
            $("#email").removeClass('is-invalid');
        }

        // remove out duplicate IPs 
        let noDuplicateIPs = []
        let allIps = mode == 'f' ? validIPs.concat(invalidIpsV) : validIPsModei;
        allIps.forEach(e => {
            if (!noDuplicateIPs.includes(e)) {
                noDuplicateIPs.push(e);
            }
        });

        $(this).prop('disabled', true).html('Pr<span class="spinner-grow spinner-grow-sm"></span>ceeding');
        $.post({
            url: '/upgradeDevicesIOS',
            data: JSON.stringify({
                net: $("input[name='net']:checked").val(),
                devi_model: $("#devi_model").val(),
                IPs: noDuplicateIPs,
                email: email
            }),
            contentType: 'application/json',
            cache: false,
            success: function(result) {
                $("#proceed_btn3").prop('disabled', false).html('Get started');
                if (result == 'EMAIL ERROR') {
                    $("#email").addClass('is-invalid');
                    // Swal.fire({
                    //     position: 'top-end',
                    //     icon: 'error',
                    //     title: 'Email was not found.',
                    //     showConfirmButton: false,
                    //     timer: 5000
                    // })
                    return;
                } else {
                    $("#email").removeClass('is-invalid');
                }

                if (result == 'OK') {
                    $("#s3_div").hide();
                    $("#s4_div").show();
                    $(".progress-bar").css('width', '100%')
                    Swal.fire({
                        icon: 'success',
                        title: "You're all set",
                        html: `Your devices are in process now. Please be on the look out for your emails (<em><b>${email}</b></em>).`,
                        showCloseButton: true,
                        allowOutsideClick: false
                    }).then(function(e) {
                        window.location.href = './';
                    });
                }
            },
        });
    });


    // var isFileNone = false;
    var mode = 'i';
    var validIPsModei = [];
    var validIPs = null;
    var invalidIpsV = null;

    var isInpFileValid = function(fileInpID) {
        let filename = $("#" + fileInpID).val().split(/(\\|\/)/g).pop();
        if (!filename.endsWith('.xlsx')) {
            return false;
        }
        return true;
    }

    var isIpRFC1918orMGMT = function(ip) {
        let firstIpAndIpMask = {
            '10.0.0.0': '255.0.0.0',
            '172.16.0.0': '255.240.0.0',
            '192.168.0.0': '255.255.0.0',
            '198.18.0.0': '255.254.0.0'
        }
        let inpIpArr = ip.split('.');
        for (let k in firstIpAndIpMask) {
            //  let firstIPArr = k.split('.');
            let maskArr = firstIpAndIpMask[k].split('.');
            let resIpArr = [];
            for (let i = 0; i < maskArr.length; i++) {
                resIpArr[i] = inpIpArr[i] & maskArr[i];
            }
            if (resIpArr.join('.') == k) {
                return true;
            }
        }
        return false;
    }

    var isInpValidIp = function(ip, net) {
        let ipRegex = /^(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])){3}$/;
        if (!ipRegex.test(ip)) {
            return false;
        }

        if (net == 'off') {
            if (isIpRFC1918orMGMT(ip)) {
                return false;
            } else {
                return true;
            }
        }

        if (net == 'on') {
            if (ip.startsWith('198.18') || ip.startsWith('198.19')) {
                return true;
            } else {
                return false;
            }
        }
    }

    var toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 5000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
});