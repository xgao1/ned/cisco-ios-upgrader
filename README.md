# Cisco IOS Upgrader

> Cisco IOS Upgrader updates a Cisco device's IOS to a specific version. Verifications including 'Host is not the specified model', 'Host IOS is already up-to-date', 'Host is out of memory', and 'Packets loss in file transmission' are implemented to ensure a safe IOS update. It support reading of IP addresses from a .xlsx file and is capable of updating multiple devices' IOS in one go.

## Run in Local Development

**> *Install Python3.6+ & virtualenv***

**> *Create & enter a virtual environment***

**> *Install dependencies***
  
    pip install -r requirements.txt

**> *Set environment variables(e.g. cmd)***
  
    # Credentials to log into the device management
    set AAA_UN=x
    set AAA_PW=x

    # Credentials of the off-net FTP and the on-net FTP
    set FTP_UN_OFF=x
    set FTP_PW_OFF=x
    set FTP_HOST_OFF=x
    set FTP_UN_ON=x
    set FTP_PW_ON=x
    set FTP_HOST_ON=x

    # Source configurations of Cisco IOS upgrading of ASR920, ISR4200, ISR4300, and ISR4400
    set SRC_F_ASR920=x
    set SRC_F_ISR4200=x
    set SRC_F_ISR4300=x
    set SRC_F_ISR4400=x

    # Hashes of the four source configurations
    set SRC_F_ASR920_HASH=x
    set SRC_F_ISR4200_HASH=x
    set SRC_F_ISR4300_HASH=x
    set SRC_F_ISR4400_HASH=x

    # Sender email credentials
    set SMTP_SERVER=x
    set SMTP_PORT=x
    set SENDER_EMAIL=x
    set SENDER_PWD=x

**> *Run the app***

    python IOS_upgrader.py

## API Calls

#### Read out and validate IP addresses in a .xlsx file &#x25BE;

    POST: /processInFile

###### REQUEST PARAMETERS

    net                  'on' or 'off'
    ip_f                 .xlsx file that contains IP addresses of the hosts whose IOS is being upgraded

###### SAMPLE RESPONSES

    {
        'IPs': ['134.6.76.162', '198.18.10.0', '198.18.10.1', '198.18.10.2', '198.18.10.3'],
        'valid_IPs': ['198.18.10.0', '198.18.10.1', '198.18.10.2', '198.18.10.3'],
        'invalid_IPs': ['134.6.76.162']
    }

#### Upgrade devices' IOS &#x25BE;

    POST: /upgradeDevicesIOS

###### REQUEST PARAMETERS
*Content-Type: application/json*

    net                  'on' or 'off'
    devi_model           'ASR-920', 'ISR-4200', 'ISR-4300', 'ISR-4400', or 'mix'

    IPs                  Unique IP addresses of the hosts that are being upgraded
    email                Email address of who will get notified in the process of upgrading

###### SAMPLE RESPONSES

    # When the email is invalid
    'EMAIL ERROR'

    # The successful output, which indicates that the upgrading is in process and two emails will be sent out as the process updates
    'OK'
