from flask import Flask, render_template, request, jsonify
import os
from utils_IOS_upgrader import read_IPs_from_xlsx, check_IPs, \
    extract_model_from_out, extract_free_bytes_from_out, extract_cp_info_from_out, \
    write_data_to_xlsx, order_results, send_email_with_att,  \
    Custom_Error, FTP_CREDENTIALS, FREE_MEMORY_LOWER_BOUND, SRC_F, SRC_F_HASH
from werkzeug.utils import secure_filename
import uuid
import netdev
import asyncio
from threading import Thread


app = Flask(__name__)

AAA_UN = os.environ['AAA_UN']
AAA_PW = os.environ['AAA_PW']

IN_FILE_FOLDER = './in_fs'
EMAIL_HEADER = '[NED - Cisco IOS Upgrader]'


# upgrade a Cisco device's IOS
async def upgrade_IOS(devi_param, net, model):
    try:
        async with netdev.create(**devi_param) as devi:

            # check if the host's model matches the model indicated in the input(i.e. ASR-920/ISR-4200/ISR-4300/ISR-4300/mix)
            out_ver = await devi.send_command("show version")
            out_ver_parsed = extract_model_from_out(out_ver)
            if model == 'mix':
                if out_ver_parsed and out_ver_parsed in SRC_F:
                    model = out_ver_parsed
                else:
                    raise Custom_Error('ERROR - Host model couldn\'t be determined')
            elif model != out_ver_parsed:
                raise Custom_Error(f'ERROR - {out_ver_parsed} host is not {model}')
            
            ftp_param = FTP_CREDENTIALS[net]
            config = SRC_F[model]
            config_hash = SRC_F_HASH[model]

            # check if the host already has the upgrade config 
            out_dir = await devi.send_command(f"dir flash: | i {config}")
            if out_dir:
                return devi_param['host'], 'Host IOS is already up-to-date', None, None

            # check if the host has enough memory to proceed upgrade
            out_dir = await devi.send_command("dir flash: | i total")
            free_bytes = extract_free_bytes_from_out(out_dir)
            if int(free_bytes) < FREE_MEMORY_LOWER_BOUND:
                raise Custom_Error('ERROR - Host is out of memory')

            ## upgrade IOS
            await devi.send_config_set(["file prompt quiet"])   # denoise
            out_cp = await devi.send_command(f"copy ftp://{ftp_param}//{config} flash:",
                                             pattern=r'bytes\/sec\)')

            # check if there was packet loss in file transmission while upgrading
            out_vr = await devi.send_command(f"verify /md5 flash:{config}", pattern='Done!')
            out_vr_parsed = out_vr.rsplit('= ', 1)
            if config_hash not in out_vr_parsed[-1]:
                await devi.send_command(f"delete flash:{config}")
                await devi.send_config_set(["file prompt noisy"])
                raise Custom_Error('ERROR - Packets loss in file transmission')

            await devi.send_config_set([f"boot system bootflash:{config}", "file prompt noisy"])
            out_wr = await devi.send_command("wr")

            out_cp_parsed = extract_cp_info_from_out(out_cp)
            out_wr_parsed = ' '.join(out_wr.split('\n\n'))

            return devi_param['host'], out_cp_parsed, out_wr_parsed, None
    except netdev.exceptions.TimeoutError as e:
        return devi_param['host'], None, None, str(e)
    except Custom_Error as e:
        return devi_param['host'], None, None, str(e)
    # except:
    #     return devi_param['host'], None, None, 'Error'


# execute tasks of IOS upgrading & send out an notification when tasks are finished
def execute_upgrading_tasks(tasks, out_order, noti_receiver):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    results = loop.run_until_complete(asyncio.gather(*tasks))

    print('------------>')
    print(results)
    print('<------------')

    # ensure the 2nd email has the same order as the 1st one in terms of IPs
    ress_ordered = order_results(results, out_order)

    # TODO: send out an email when tasks are finished
    tmp_file = os.path.join(IN_FILE_FOLDER, str(uuid.uuid4()) + '.xlsx')
    write_data_to_xlsx(ress_ordered, tmp_file, ls_item=True)
    noti_dtls = {
        # 'fake_from': 'XGao@Fake.com',
        'to': [noti_receiver],
        'subject': f'{EMAIL_HEADER} - Finished Upgrading',
        'content': '''
                   <font size="5" face="arial" color="blue"><b><i>Your earlier upgrading was finished...</i></b>⬇️⬇️⬇️</font>
                   <br><br><br>
                   <font size="4" face="arial"><b>*** Devices that failed upgrading would be caught in the attached report ***</b></font>
                   <br><br><br>
                   <font size="4" face="arial">See the attachment for the report on your upgrading.</font>''',
        'att': {
            'path': tmp_file,
            'name': f'Finished_{uuid.uuid4()}.xlsx'
        }
    }
    send_email_with_att(noti_dtls, html=True)
    os.remove(tmp_file)


@app.route('/')
def index():
    return render_template('index.html')


# in_file format: .xlsx
@app.route('/processInFile', methods=['POST'])
def process_in_file():
    net = request.args.get('net')
    f = request.files['ip_f']

    path_f = os.path.join(IN_FILE_FOLDER, str(
        uuid.uuid4()) + '_' + secure_filename(f.filename))
    f.save(path_f)
    IPs = read_IPs_from_xlsx(path_f)
    valid_IPs, invalid_IPs = check_IPs(
        IPs) if net == 'on' else check_IPs(IPs, on_net=False)
    os.remove(path_f)

    resp_data = {
        'IPs': list(IPs),
        'valid_IPs': valid_IPs,
        'invalid_IPs': invalid_IPs
    }
    print('<----------')
    print(resp_data)
    print('----------->')
    return jsonify(resp_data)


@app.route('/upgradeDevicesIOS', methods=['POST'])
def upgrade_devis_IOS():
    req_data = request.get_json()
    net = req_data['net']
    devi_model = req_data['devi_model']
    IPs = req_data['IPs']    # unique IPs
    noti_receiver = req_data['email']

    print('<-------------')
    print(devi_model)
    print(net)
    print(IPs)
    print('-------------->')

    # send out an email notification
    tmp_file = os.path.join(IN_FILE_FOLDER, str(uuid.uuid4()) + '.xlsx')
    write_data_to_xlsx(IPs, tmp_file)
    noti_dtls = {
        # 'fake_from': 'XGao',
        'to': [noti_receiver],
        'subject': f'{EMAIL_HEADER} - in Progress Upgrading',
        'content': '''
            <font size="6" face="arial" color="blue"><b><i>Your upgrading is in process now...</i></b>🍵🍵🍵</font>
            <br><br><br>
            <font size="4" face="arial">This account will get notified again once your upgrading has been finished.</font>
            <br><br>
            <font size="4" face="arial">See the attachment for the devices that are being upgraded.</font>''',
        'att': {
            'path': tmp_file,
            'name': f'in_Process_{uuid.uuid4()}.xlsx'
        }
    }
    try:
        send_email_with_att(noti_dtls, html=True)
    except:
        # used for the front end to doubly verify the email input
        return 'EMAIL ERROR'
    finally:
        os.remove(tmp_file)

    tasks = []
    for IP in IPs:
        devi = {
            'username': AAA_UN,
            'password': AAA_PW,
            'device_type': 'cisco_ios',
            'host': IP
        }
        tasks.append(upgrade_IOS(devi, net, devi_model))
    thread = Thread(target=execute_upgrading_tasks,
                    args=(tasks, IPs, noti_receiver))
    thread.start()
    return 'OK'


if __name__ == '__main__':
    app.run(port=5001, debug=True)
